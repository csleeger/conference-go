import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_location_img(state, city):
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    query = f"{city}, {state}"
    params = {"query": query}

    r = requests.get(url, headers=headers, params=params)

    if r.status_code == 200:
        data = r.json()

        if data["photos"]:
            return data["photos"][0]["url"]
        else:
            return None
    else:
        return None


def get_coordinates(city, abbreviation):
    url = "http://api.openweathermap.org/geo/1.0/direct"

    country = "US"
    query = f"{city},{abbreviation},{country}"
    querystring = {"q": query, "appid": OPEN_WEATHER_API_KEY}

    payload = ""
    headers = {"User-Agent": "insomnia/8.5.1"}

    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    if response.status_code == 200:
        data = response.json()

        latlon = {}

        if data and "lat" in data[0] and "lon" in data[0]:
            latlon["lat"] = data[0]["lat"]
            latlon["lon"] = data[0]["lon"]
            return latlon
        else:
            return None
    else:
        return None

    # print(latlon)
    # print("Data")
    # print(data)
    # print("response.text")
    # print(response.text)
    # print("latlon")
    # print(latlon)

def get_weather(latlon):
    url = "https://api.openweathermap.org/data/2.5/weather"

    lat = latlon["lat"]
    lon = latlon["lon"]

    querystring = {"lat": lat, "lon": lon, "appid": OPEN_WEATHER_API_KEY, "units": "imperial"}

    payload = ""
    headers = {"User-Agent": "insomnia/8.5.1"}

    response = requests.request("GET", url, data=payload, headers=headers, params=querystring)

    if response.status_code == 200:
        data = response.json()

        weather = {}

        if data:
            weather["temp"] = data["main"]["temp"]
            weather["description"] = data["weather"][0]["description"]
            return weather
        else:
            return None
    else:
        return None

    # print("weather")
    # print(weather)

    # # print("Data")
    # # print(data)
    # print("response.text")
    # print(response.text)
